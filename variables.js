/**
 * Var, let, const
 * Var: the scope is the function, where the variable declared in
 * let: the scope is the code block, where the variable declared in
 * const: the scope is the block, where the variable is set, and the variable can't be changed any more
 */

//we use 'var' here to define a global setting, accessible from any part of our program,
//this variable can be reassigned in any part of our program too
var time_format = 'sec';

/**
 * This function returns current timestamp in seconds or milliseconds depending on the time_format setting
 * @returns {number}
 */
function getTime()
{
    //сonst to define 2 acceptable formats of global setting time_format, that must be recognizable in this func
    //the value of these consts cannot be changed after declaration
    const time_format_seconds = 'sec';
    const time_format_milliseconds = 'millisec';

    //just local variable to get raw timestamp in milliseconds, it should not be accessible outside of our function
    let timestamp = Date.now();
    if (time_format === time_format_milliseconds) {
        return timestamp;
    }
    else if (time_format === time_format_seconds) {
        return Math.floor(timestamp / 1000);
    }
    else {
        throw 'Unknown timestamp format';
    }
}

console.log(getTime());

time_format = 'millisec';

console.log(getTime());

/*
    if the next block is uncommented we will have a error, because time format constants are not visible in the global scope,
    but that's an idea to move out consts to the global scope to lem them be used not only in getTime func
*/
/*
time_format = time_format_seconds;
console.log(getTime());
*/

/*
    local variable timestamp from getTime func is not accessible there,
    but it would have been accessible if had been declared without any keyword (bad practice)
 */
//console.log(timestamp);

