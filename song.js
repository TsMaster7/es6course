/**
 * Some info about song "May be I may be you" by Scorpions
 * to illustrate JS data types
 */

//scalar types for general info
var title = 'May be I may be you';
var artist = 'Scorpions';
var genre = 'Rock';
var durationInSeconds = 212;
var yearOfProduce = 2004;
var composer = 'Anoushiravan Rohani';
var lyricsBy = 'Klaus Meine';
var album = 'Unbreakable';

//detailed info about band collected in object
var artistDetailedInfo = {
    'type': 'band',
    'originCity': 'Hannover',
    'created': 1965,
    'members': [
        'Rudolf Schenker',
        'Klaus Meine',
        'Matthias Jabs',
        'Pawel Mąciwoda',
        'Mikkey Dee',
    ],
    'genres': [
        'rock',
        'metal',
        'pop',
    ],
    'isActiveNow': true
};

//let's print the whole information we have
console.log("Title: " + title);
console.log("Artist: " + artist);
console.log("Genre: " + genre);
console.log("Duration: " + durationInSeconds + "sec");
console.log("Year: " + yearOfProduce);
console.log("Composer: " + composer);
console.log("Lyrics by: " + lyricsBy);
console.log("Album: " + album);

console.log("Band details:");
console.log(artistDetailedInfo);